FROM ubuntu:16.04

RUN apt-get update && apt-get -y upgrade

# install requered Packages for yocto
# Essentials: Packages needed to build an image on a headless system:
RUN apt-get -y install gawk wget git-core diffstat unzip texinfo gcc-multilib \
	build-essential chrpath socat \
	cpio python python3 python3-pip python3-pexpect \
	xz-utils debianutils iputils-ping

# Graphical and Eclipse Plug-In Extras: Packages recommended if the host system has graphics support or if you are going to use the Eclipse IDE:
#RUN apt-get -y install libsdl1.2-dev xterm

# xxd is used while fit image creation
RUN apt-get --no-install-recommends -y install vim-common

# Documentation: Packages needed if you are going to build out the Yocto Project documentation manuals:
RUN apt-get --no-install-recommends -y install make xsltproc docbook-utils fop dblatex xmlto

# OpenEmbedded Self-Test (oe-selftest): Packages needed if you are going to run oe-selftest:
RUN apt-get --no-install-recommends -y install python-git

# prevent heimdal error
RUN cpan JSON

# install additional dependencies
RUN apt-get --no-install-recommends -y install libudev-dev

# add repo tool
RUN apt-get --no-install-recommends -y install curl
RUN curl http://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
RUN chmod a+x /usr/local/bin/repo

# add xmlstarlet to modify repo manifest
RUN apt-get --no-install-recommends -y install xmlstarlet

# install jq for deploy job
RUN apt-get --no-install-recommends -y install jq

# create user for yocto builds
RUN useradd --uid 1000 --create-home build 

# use build user for yocto builds
USER build
WORKDIR /home/build
